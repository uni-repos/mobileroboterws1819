#! /usr/bin/env python
import rospy
import actionlib
import time
import math
from random import randint
from std_srvs.srv import Empty
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal


asca = """
  _____            _____  _    _          ______ ____  _____ _______ _    _ _   _          
 |  __ \     /\   |  __ \| |  | |        |  ____/ __ \|  __ \__   __| |  | | \ | |   /\    
 | |__) |   /  \  | |__) | |__| | __  __ | |__ | |  | | |__) | | |  | |  | |  \| |  /  \   
 |  _  /   / /\ \ |  ___/|  __  | \ \/ / |  __|| |  | |  _  /  | |  | |  | | . ` | / /\ \  
 | | \ \  / ____ \| |    | |  | |  >  <  | |   | |__| | | \ \  | |  | |__| | |\  |/ ____ \ 
 |_|  \_\/_/    \_\_|    |_|  |_| /_/\_\ |_|    \____/|_|  \_\ |_|   \____/|_| \_/_/    \_\ 
         v1.0                                                                              
"""

class PatrolPosition():
	def __init__(self, x, y, z=0, ox=0, oy=0, oz=0, ow=1, frame="map"):
		self.x = x
		self.y = y
		self.z = z
		self.ox = ox
		self.oy = oy
		self.oz = oz
		self.ow = ow
		self.frame = frame

	def buildGoal(self):
		goal = MoveBaseGoal()
		goal.target_pose.header.stamp = rospy.get_rostime()
		goal.target_pose.header.frame_id = self.frame
		goal.target_pose.pose.position.x = self.x
		goal.target_pose.pose.position.y = self.y
		goal.target_pose.pose.position.z = self.z

		goal.target_pose.pose.orientation.x = self.ox
		goal.target_pose.pose.orientation.y = self.oy
		goal.target_pose.pose.orientation.z = self.oz
		goal.target_pose.pose.orientation.w = self.ow

		return goal

def moveTo(position):
	goal = position.buildGoal()
	client.send_goal(goal)
  	client.wait_for_result()

def getNextPos(exclude, max):
	result = -1

	while result == -1 or result == exclude:
		result = randint(0, max)
		rospy.loginfo('Random: {}'.format(result))

	rospy.loginfo('Return: {}'.format(result))
	return result



# Main program

print(asca)

rospy.init_node('patrol_node')
client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
client.wait_for_server()

rospy.loginfo('Initialized Patrol Node')

posList = []
posList.append(PatrolPosition(-1.47, 0.8, oz=0.85, ow=0.5))
posList.append(PatrolPosition(-0.3, 5.1, oz=-0.85, ow=0.53))
posList.append(PatrolPosition(-3.8, 7.2, oz=-0.33, ow=0.95))
posList.append(PatrolPosition(-7, 4, oz=-0.22))

rospy.loginfo('Initialized Positions')
rospy.loginfo('Start patroling')


# Random
lastPos = -1
while not rospy.is_shutdown():
	nextPos = getNextPos(lastPos, len(posList) - 1)
	rospy.loginfo('Driving to pos {}: {} / {}'.format(nextPos, posList[nextPos].x, posList[nextPos].y))
	rospy.loginfo('Clearing Costmap')
	costAnswer = rospy.ServiceProxy('move_base/clear_costmaps', Empty)
	costAnswer()
	moveTo(posList[nextPos])
	lastPos = nextPos
	time.sleep(2)




# Non Random (OLD)
# while not rospy.is_shutdown():
#	for pos in posList:
#		rospy.loginfo('Driving to: {} / {}'.format(pos.x, pos.y))
#		rospy.loginfo('Clearing Costmap')
#		costAnswer = rospy.ServiceProxy('move_base/clear_costmaps', Empty)
#		costAnswer()
#		moveTo(pos)
#		time.sleep(5)
#rospy.spin()