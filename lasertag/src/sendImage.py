#!/usr/bin/env python
import message_filters
import rospy
from rpc_game_client.srv import PlayerScore
from sensor_msgs.msg import CompressedImage, CameraInfo
from apriltags2_ros.msg import AprilTagDetectionArray

class Scoring:
    def __init__(self):
        rospy.init_node("apriltag_scoring")
        # initalisieren des Times mit -20s
        self.last_score_time = rospy.get_rostime() - rospy.Duration(20.0)

        image_sub = message_filters.Subscriber('/camera/rgb/image_color/compressed', CompressedImage)
        info_sub = message_filters.Subscriber('/camera/rgb/camera_info', CameraInfo)
        apriltag_sub = message_filters.Subscriber("/tag_detections", AprilTagDetectionArray)

        ts = message_filters.TimeSynchronizer([image_sub, info_sub, apriltag_sub], 10)
        ts.registerCallback(self.callback)
    
        rospy.spin()

    def send_score_image(self, image, camera_info):
        rospy.wait_for_service('rpc_score')
        try:
            player_score = rospy.ServiceProxy('rpc_score', PlayerScore)
            resp = player_score(image, camera_info)
            rospy.loginfo(resp)
        except rospy.ServiceException, e:
             print "Service call failed: %s"%e

    def callback(self, image, camera_info, apriltag):
        # gucken ob im bild ein apriltag ist
        if len(apriltag.detections) > 0:
            # pruefen ob der Apriltag nah genu ist um eine erkennung zu gerwaehrleisten
            if apriltag.detections[0].pose.pose.pose.position.z < 1.5:
                
                # wenn ja und seit letztem schiessen 10 sekunden vergangen sind: schiessen
                if (rospy.get_rostime() - self.last_score_time) >= rospy.Duration(10):
                    # schiessen = send_score_image aufrufen
                    rospy.loginfo(apriltag.detections[0].pose)
                    self.send_score_image(image, camera_info)
                    self.last_score_time = rospy.get_rostime()
        
 
if __name__ == "__main__":
    Scoring()
