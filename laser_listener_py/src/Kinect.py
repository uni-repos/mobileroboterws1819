#!/usr/bin/env python
import rospy

from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist


	
def listener():
	rospy.init_node('laserlistener')
	pub = rospy.Publisher('cmd_vel_mux/input/teleop', Twist)
	rate = rospy.Rate(50)

	def stop():
		
		pub.publish(Twist())
		print("stop right there criminal scum!")

	def dosmth(range):
	
		twist = Twist()
		
		vel = stdVel

		if range < (stoppingDis + stopLength):
			diff = range - stoppingDis
			vel = max(minVel, (diff / stopLength) * stdVel)
			print("vel: {}".format(vel))
		twist.linear.x = vel
		pub.publish(twist)


	def callback(data):
		# initialize with big Number to stop when there is no readable data
		lowest_range = 20.0
	
		ranges = data.ranges
	
		length = len(data.ranges)

		for i in range(length):
			if ranges[i] < lowest_range:
				lowest_range = ranges[i]


		print("minrange: {}".format(lowest_range))

		if lowest_range > stoppingDis and lowest_range < 20:
			dosmth(lowest_range)

		else:
			stop()

		rate.sleep()


	rospy.Subscriber("kinect_scan", LaserScan, callback)
	rospy.spin()
	

if __name__ == '__main__':
	
	stoppingDis = 1
	stopLength = 0.2
	stdVel = 0.2
	minVel = 0.05
	
	listener()

