#!/usr/bin/env python

import math
import rospy
from geometry_msgs.msg import Twist

def talker():
    pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
   
	twist = Twist()
	rate.sleep()

        twist.linear.x = 1 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()

        twist.angular.z = math.pi/2
        pub.publish(twist)
        rate.sleep()
        twist.angular.z = 0
     	pub.publish(twist)
        rate.sleep()

	twist.linear.x = 1 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()

	twist.angular.z = math.pi/6
        pub.publish(twist)
        rate.sleep()
        twist.angular.z = 0
     	pub.publish(twist)
        rate.sleep()

	twist.linear.x = 1 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()
	
	twist.angular.z = math.pi/2 + math.pi/6
        pub.publish(twist)
        rate.sleep()
        twist.angular.z = 0
     	pub.publish(twist)
        rate.sleep()

	twist.linear.x = 1 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()

	twist.angular.z = math.pi/6
        pub.publish(twist)
        rate.sleep()
        twist.angular.z = 0
     	pub.publish(twist)
        rate.sleep()

	twist.linear.x = 1 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()

	twist.angular.z = math.pi/2 + math.pi/4
        pub.publish(twist)
        rate.sleep()
        twist.angular.z = 0
     	pub.publish(twist)
        rate.sleep()

	twist.linear.x = math.sqrt(2) 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()

     	twist.angular.z = math.pi/2 + math.pi/4
        pub.publish(twist)
        rate.sleep()
        twist.angular.z = 0
     	pub.publish(twist)
        rate.sleep()

	twist.linear.x = 1 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()

	twist.angular.z = math.pi/2 + math.pi/4
        pub.publish(twist)
        rate.sleep()
        twist.angular.z = 0
     	pub.publish(twist)
        rate.sleep()

	twist.linear.x = math.sqrt(2) 
        pub.publish(twist)
        rate.sleep()
        twist.linear.x = 0
	pub.publish(twist)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
